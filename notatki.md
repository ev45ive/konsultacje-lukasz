## Instalcje 
node -v
npm -v
git --version

code -v 
chrome://version

## NPM
https://semver.npmjs.com/
https://semver.org/ 

## React

https://pl.reactjs.org/docs/getting-started.html
https://pl.reactjs.org/tutorial/tutorial.html
https://pl.reactjs.org/docs/add-react-to-a-website.html

https://create-react-app.dev/docs/getting-started
https://github.com/gsoft-inc/craco

# Stara metoda
npm i -g create-react-app
create-react-app --help

## Uninstall
npx create-react-app "." --template typescript --use-npm 

You are running `create-react-app` 4.0.3, which is behind the latest release (5.0.0).

We no longer support global installation of Create React App.

Please remove any global installs with one of the following commands:
- npm uninstall -g create-react-app
- yarn global remove create-react-app

The latest instructions for creating a new app can be found here:
https://create-react-app.dev/docs/getting-started/


## Nowa (zalecana) metoda tworzenia aplikacji w React
npx create-react-app --help
Usage: create-react-app <project-directory> [ options ]

Options:
  -V, --version                            output the version number
  --verbose                                print additional logs
  --info                                   print environment debug info
  --scripts-version <alternative-package>  use a non-standard version of react-scripts
  --template <path-to-template>            specify a template for the created project
  --use-npm
  --use-pnp
  -h, --help                               output usage information
    Only <project-directory> is required.

    A custom --scripts-version can be one of:
      - a specific npm version: 0.8.2
      - a specific npm tag: @next
      - a custom fork published on npm: my-react-scripts
      - a local path relative to the current working directory: file:../my-react-scripts
      - a .tgz archive: https://mysite.com/my-react-scripts-0.8.2.tgz
      - a .tar.gz archive: https://mysite.com/my-react-scripts-0.8.2.tar.gz
    It is not needed unless you specifically want to use a fork.

    A custom --template can be one of:
      - a custom template published on npm: cra-template-typescript
      - a local path relative to the current working directory: file:../my-custom-template
      - a .tgz archive: https://mysite.com/my-custom-template-0.8.2.tgz
      - a .tar.gz archive: https://mysite.com/my-custom-template-0.8.2.tar.gz

    If you have any problems, do not hesitate to file an issue:
      https://github.com/facebook/create-react-app/issues/new


PC@DESKTOP-6HIPUQN MINGW64 /c/Projects/lukasz/nowa-aplikacja
$ npx create-react-app --version
4.0.3

## Instalacja
npx create-react-app "." --template typescript --use-npm
npx: zainstalowano 67 w 5.92s

Creating a new React app in C:\Projects\lukasz\nowa-aplikacja.

Installing packages. This might take a couple of minutes.
Installing react, react-dom, and react-scripts with cra-template-typescript...


We detected TypeScript in your project (src\App.test.tsx) and created a tsconfig.json file for you.

Your tsconfig.json has been populated with default values.


Created git commit.

Success! Created nowa-aplikacja at C:\Projects\lukasz\nowa-aplikacja
Inside that directory, you can run several commands:

  npm start
    Starts the development server.

  npm run build
    Bundles the app into static files for production.

  npm test
    Starts the test runner.

  npm run eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

We suggest that you begin by typing:

  cd C:\Projects\lukasz\nowa-aplikacja
  npm start

Happy hacking!

## Start dev server

npm run start

> nowa-aplikacja@0.1.0 start C:\Projects\lukasz\nowa-aplikacja
> react-scripts start

Compiled successfully!

You can now view nowa-aplikacja in the browser.  

  http://localhost:3000

Note that the development build is not optimized.
To create a production build, use npm run build. 

assets by path static/ 1.49 MiB
  asset static/js/bundle.js 1.48 MiB [emitted] (name: main) 1 related asset
  asset static/js/node_modules_web-vitals_dist_web-vitals_js.chunk.js 6.89 KiB [emitted] 1 related asset
  asset static/media/logo.6ce24c58023cc2f8fd88fe9d219db6c6.svg 2.57 KiB [emitted] (auxiliary name: main)
asset index.html 1.67 KiB [emitted]
asset asset-manifest.json 546 bytes [emitted]
runtime modules 31.3 KiB 15 modules
modules by path ./node_modules/ 1.35 MiB 90 modules
modules by path ./src/ 18.1 KiB
  modules by path ./src/*.css 8.82 KiB
    ./src/index.css 2.72 KiB [built] [code generated]
    ./node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[1].oneOf[5].use[1]!./node_modules/postcss-loader/dist/cjs.js??ruleSet[1].rules[1].oneOf[5].use[2]!./node_modules/source-map-loader/dist/cjs.js!./src/index.css 1.37 KiB [built] [code generated]
    ./src/App.css 2.72 KiB [built] [code generated]
    ./node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[1].oneOf[5].use[1]!./node_modules/postcss-loader/dist/cjs.js??ruleSet[1].rules[1].oneOf[5].use[2]!./node_modules/source-map-loader/dist/cjs.js!./src/App.css 2 KiB [built] [code generated]
  modules by path ./src/*.tsx 4.31 KiB
    ./src/index.tsx 1.79 KiB [built] [code generated]
    ./src/App.tsx 2.52 KiB [built] [code generated]
  ./src/reportWebVitals.ts 1.39 KiB [built] [code generated]
  ./src/logo.svg 3.61 KiB [built] [code generated]
webpack 5.65.0 compiled successfully in 16855 ms
No issues found.


## TsConfig

    "isolatedModules": true, // Tylko moduly
    // modul musi miec minimum 1 import "..."; lub export {...}

    "lib": ["esnext", ...] // allow this
    "target": "es2015", /// emit this
    
    "allowJs": true,


## React components + Storybook
https://storybook.js.org/docs/react/get-started/whats-a-story
http://localhost:6006/?path=/docs/components-listgroup--primary


## Ext
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

tsrafc Ctrl+Space Enter

https://react-bootstrap.github.io/components/alerts
https://getbootstrap.com/docs/5.1/components/card/


## GIT
git clone https://bitbucket.org/ev45ive/konsultacje-lukasz.git konsultacje-lukasz
cd konsultacje-lukasz
npm i 

npm run start
npm run storybook


