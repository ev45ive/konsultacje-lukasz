// tsrafc
import React from "react";

interface Props {
  items: string[];
  title?: string;
}

export const ListGroup = (props: Props) => {
  return (
    <div>
      <ul className="list-group">
        {props.items.map((item) => (
          <li className="list-group-item">{item}</li>
        ))}
      </ul>
    </div>
  );
};
