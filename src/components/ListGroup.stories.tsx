import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { ListGroup } from "./ListGroup";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Components/ListGroup",
  component: ListGroup,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    // backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof ListGroup>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof ListGroup> = (args) => (
  <ListGroup {...args} />
);

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  items: ["Ala ", "ma", "kota"],
};

// export const Secondary = Template.bind({});
// Secondary.args = {
//   label: 'ListGroup',
// };
