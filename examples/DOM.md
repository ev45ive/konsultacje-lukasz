```js

div = document.createElement('div')
<div>​</div>​

p = document.createElement('p')
<p>​</p>​
p.innerText = 'Ala ma kota'
'Ala ma kota'
div.append(p)

div 
<div>​<p>​Ala ma kota​</p>​</div>​
document.body.append(div)

root 
// <div id=​"root">​</div>​
root.append(div)

document.getElementById('root')
// <div id=​"root">​…​</div>​
root = 123
123

document.getElementById('root').append(div)

div 
// <div>​…​</div>​
div.innerText = '<h1>test</h1>'

'<h1>test</h1>'
div.innerHTML = '<h1>test</h1>'


div.innerHTML = `<div>
    <p>Ala ma <b>kota<b></p>
</div>`
'<div>\n    <p>Ala ma <b>kota<b></p>\n</div>'

    
//<div>​…​</div>​<div>​<p>​"Ala ma "<b>​"kota"<b>​</b>​</b>​</p>​<b>​<b>​ ​</b>​</b>​</div>​</div>
​
div.innerHTML = `<div>
    <span>Ala ma <b>kota<b></span>
    i <input>
</div>`

'<div>\n    <span>Ala ma <b>kota<b></span>\n    i <input>\n</div>'
div.innerHTML = `<div>
    <span>Ala ma <b>psa</b></span>
    i <input>
</div>`

'<div>\n    <span>Ala ma <b>psa</b></span>\n    i <input>\n</div>'
$0.style = 'color:red'
'color:red'
$0.style = 'border-color:red'
'border-color:red'


//<div style=​"border-color:​ red;​">​…​</div>​
div.children[0].children[1].value = 'test'
'test'

$0.style.border = '1px solid red'
'1px solid red'

$0.style.color = 'blue'
'blue'

```