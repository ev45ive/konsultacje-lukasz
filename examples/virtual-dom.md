```jsx 

div = document.createElement('div')
vdiv = React.createElement('div')
// { 
//     $$typeof: Symbol(react.element)
//     key: null
//     props: {}
//     ref: null
//     type: "div"
// }
// Immutable

vdiv = React.createElement('div',{
    id:'test123', style:'test'
})
// {$$typeof: Symbol(react.element), type: 'div', key: null, ref: null, props: {…}, …}
div.class 
// undefined
div.className
// ''

vdiv = React.createElement('div',{
    id:'test123', 
    style:{ borderColor:'red' }, 
    className:'testclass' 
},
     React.createElement('span',null, 'Ala ma kota'),
     React.createElement('input',null),
)

{
    $$typeof: Symbol(react.element)
    key: null
    props: {
        children: Array(2)
            0: {$$typeof: Symbol(react.element), type: 'span', key: null, ref: null, props: {…}, …}
            1: {$$typeof: Symbol(react.element), type: 'input', key: null, ref: null, props: {…}, …}
            length: 2
        className: "testclass"
        id: "test123"
        style: {borderColor: 'red'}
    }
}

vdiv = React.createElement('div',{
    id:'test123', 
    style:{ color:'blue' }, 
    className:'testclass' 
},
     React.createElement('span',null, 'Bob ma psa'),
     React.createElement('input',null),
)

ReactDOM.render(vdiv, root)
// <div id=​"test123" class=​"testclass" style=​"color:​ blue;​">​<span>​Bob ma psa​</span>​<input>​…​</input>​</div>​
vdiv
// {$$typeof: Symbol(react.element), type: 'div', key: null, ref: null, props: {…}, …}$$typeof: Symbol(react.element)key: nullprops: {id: 'test123', style: {…}, className: 'testclass', children: Array(2)}ref: nulltype: "div"_owner: null_store: {validated: false}_self: null_source: null[[Prototype]]: Object
document.createElement('div')
// <div>​</div>​
root 
// <div id=​"root">​<div id=​"test123" class=​"testclass" style=​"color:​ blue;​">​…​</div>​</div>​
$0.__reactContainer$ytaodgiakif
// FiberNode {tag: 3, key: null, elementType: null, type: null, stateNode: FiberRootNode, …}