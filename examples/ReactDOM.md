```js



window.React = React;
window.ReactDOM = ReactDOM;

interface Person {
  id: string;
  name: string;
  color: string;
  pet: {
    name: string;
  };
}

const users: Person[] = [
  { id: "123", name: "Alice", color: "red", pet: { name: "Cat" } },
  { id: "234", name: "Bob", color: "blue", pet: { name: "Dog" } },
  { id: "345", name: "Cat", color: "hotpink", pet: { name: "Parrot" } },
];

const Person = (props: { user: Person }) =>
  React.createElement(
    "span",
    {
      style: { color: props.user.color },
    },
    React.createElement(
      "span",
      null,
      `${props.user.name} as a ${props.user.pet.name}`
    )
  );

const PeopleList = (props: { users: Person[] }) =>
  React.createElement(
    "ul",
    { className: "list-group" },
    props.users.map((user) => {
      return React.createElement(
        "li",
        {
          className: "testclass",
        },
        Person({ user: user })
      );
    })
  );

ReactDOM.render(PeopleList({ users }), document.getElementById("root"));


```